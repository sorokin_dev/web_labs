<?php
$pageTitle = 'Personal';
$selectedMenuItem = 'personal';

require_once "blocks/header.php";
?>

<div class="content">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 text-center">
                <h1>
                    Личный кабинет
                </h1>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/vova.jpg" class="img-fluid"> <!-- Картинка юзера -->
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">Имя Vova</h4>
                        <p class="card-text">
                            О себе
                            fsfsdfgdgdgsdgdfgdfsgdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfg
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-12 text-center">
                <form action="error.php">
                    <button type="submit" class="btn btn-primary w-25 mt-5 bg-danger">Exit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'blocks/footer.php';
?>