<?php
$pageTitle = 'About';
$selectedMenuItem = 'about';

require_once "blocks/header.php";
?>

<div class="container ">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">
                Автосалон Вода
            </h1>
        </div>

        <div class="col-2"></div>
        <div class="col-8 about-description">
            <p class="left">
                Мы официальный дилер BMW в Волгу! В Дилерском центре Вода весь модельный ряд в наличии с ПТС!Мы
                являемся официальными дилерами BMW более 2 недель. Многолетний опыт работы помогает нам уделить внимание
                даже самым маленьким деталям, которые важны при покупке автомобиля BMW, а также при его дальнейшем
                техническом обслуживании.
                <br><br>
                В нашем автосалоне вы сможете не только приобрести автомобиль, но установить дополнительное
                оборудование, докупить оригинальные аксессуары и запасные части BMW, сделать шиномонтаж в зимний сезон
                и обработать автомобиль антикоррозийным покрытием!
                <br><br>
                Специалисты сервисного центра всегда проконсультируют вас по любым вопросам,касающихся конструктивных
                особенностей автомобиля, его технического обслуживания, гарантийного и текущего ремонта, заказа запасных
                частей и оригинальных аксессуров
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card">
                <div class="card-img">
                    <img src="static/images/vova.jpg" class="img-fluid about-img">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Вова Сорокин</h4>
                    <p class="card-text">
                        Middle Flutter dev
                    </p>
                </div>
                <div class="card-footer">
                    <a href="https://vk.com/sorokin_dev" class="card-link">Вк</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card">
                <div class="card-img">
                    <img src="static/images/danya.jpg" class="img-fluid about-img">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Даниил Тимонин</h4>
                    <p class="card-text">
                        Middle Android dev
                    </p>
                </div>
                <div class="card-footer">
                    <a href="https://vk.com/timonin_dev" class="card-link">Вк</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'blocks/footer.php';
?>