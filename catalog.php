<?php
$pageTitle = 'Catalog';
$selectedMenuItem = 'catalog';

require_once "blocks/header.php";
?>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">Каталог</h1>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/bmw.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">BMW X5</h4>
                        <p class="card-text">
                            Новый спортивный внедорожник. Он включает в себя систему xDrive с мощными дизельными и
                            бензиновыми двигателями BMW.
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="" class="card-link">Смотреть</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/bmw.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">BMW X5</h4>
                        <p class="card-text">
                            Новый спортивный внедорожник. Он включает в себя систему xDrive с мощными дизельными и
                            бензиновыми двигателями BMW.
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="" class="card-link">Смотреть</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/bmw.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">BMW X5</h4>
                        <p class="card-text">
                            Новый спортивный внедорожник. Он включает в себя систему xDrive с мощными дизельными и
                            бензиновыми двигателями BMW.
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="" class="card-link">Смотреть</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/bmw.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">BMW X5</h4>
                        <p class="card-text">
                            Новый спортивный внедорожник. Он включает в себя систему xDrive с мощными дизельными и
                            бензиновыми двигателями BMW.
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="" class="card-link">Смотреть</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/bmw.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">BMW X5</h4>
                        <p class="card-text">
                            Новый спортивный внедорожник. Он включает в себя систему xDrive с мощными дизельными и
                            бензиновыми двигателями BMW.
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="" class="card-link">Смотреть</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 catalog-item">
                <div class="card">
                    <div class="card-img">
                        <img src="static/images/bmw.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">BMW X5</h4>
                        <p class="card-text">
                            Новый спортивный внедорожник. Он включает в себя систему xDrive с мощными дизельными и
                            бензиновыми двигателями BMW.
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="" class="card-link">Смотреть</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'blocks/footer.php';
?>