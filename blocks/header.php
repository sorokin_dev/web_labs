<?php
    $menuItems = [
        'home' => [
            'href' => '/index.php',
            'title' => 'Home'
        ],
        'catalog' => [
            'href' => '/catalog.php',
            'title' => 'Catalog'
        ],
        'about' => [
            'href' => '/about.php',
            'title' => 'About'
        ],
    ];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?=$pageTitle?></title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
              integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
                integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
                crossorigin="anonymous"></script>
        <link rel="stylesheet" href="/static/styles/main.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <?php
                        foreach ($menuItems as $menuItemKey => $menuItem) {
                            $isSelected = $menuItemKey == $selectedMenuItem;
                            echo "<li class=\"nav-item " . ($isSelected ? 'active' : '') . "\">";
                                echo "<a class=\"nav-link\" href=\"${menuItem['href']}\">${menuItem['title']}</a>";
                            echo "</li>";
                        }
                    ?>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/auth.php">Sign in</a>
                    </li>
                </ul>
            </div>
        </nav>
