<?php
$pageTitle = 'Auth';
$selectedMenuItem = 'auth';

require_once "blocks/header.php";
?>

<div class="content">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <form action="personal.php"><!-- Вписать твой хандлер и делать редирект на personal.html-->
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php
    require_once 'blocks/footer.php';
?>